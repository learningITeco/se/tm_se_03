package ru.potapov.tm.repository;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CommandOfTerminal {
    private Project             currentProject;
    private DateTimeFormatter   ft;

    //Constants
    private final String YY = "Y";
    private final String Yy = "y";

    public CommandOfTerminal() {
        currentProject = null;
        ft  = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    private void printTasksOfProject(Project project, List<Task> listTasks){
        System.out.println();
        System.out.println("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (task.getIdProject().equals(project.getId())){
                System.out.println();
                System.out.println("    Task [" + task.getName() + "]");
                System.out.println("    Description: " + task.getDescription());
                System.out.println("    ID: " + task.getId());
                System.out.println("    Date start: " + task.getDateStart().format(ft));
                System.out.println("    Date finish: " + task.getDateFinish().format(ft));
            }
        }
    }

    public void commandDeleteTask(List<Task> listTasks, Scanner in) {
        if (listTasks.size() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for remove:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            for (Task task : listTasks) {
                if (task.getName().equals(name)) {
                    findTask = task;
                }
            }

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        listTasks.remove(findTask);
        System.out.println("The task has deleted");
    }

    public void commandUpdateTask(List<Task> listTasks, List<Project> listProjects, Scanner in) {
        if (listTasks.size() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for update:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            for (Task task : listTasks) {
                if (task.getName().equals(name)) {
                    findTask = task;
                }
            }

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = in.nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input a new name for this task:");
                findTask.setName(in.nextLine());
                System.out.println("Completed");
        }

        System.out.println("Do you want to update the project for this task? <y/n>");
        answer = in.nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input the name of project for this task:");

                Project findProject = null;
                String name = in.nextLine();
                for (Project project : listProjects) {
                    if (project.getName().equals(name)){
                        findProject = project;
                    }
                }

                if (Objects.isNull(findProject)){
                    System.out.println("Project with name [" + name + "] is not exist, project for this task does not changed");
                }else {
                    findTask.setIdProject(findProject.getId());
                    System.out.println("Completed");
                }
        }
    }

    public void commandDeleteAllTasks(List<Task> listTasks) {
        listTasks.clear();
        System.out.println("All tasks of all projects have deleted");
    }

    public void commandDeleteTasks(List<Task> listTasks, List<Project> listProjects, Scanner in) {
        if (listTasks.size() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing its tasks:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;
            for (Project project : listProjects) {
                if (project.getName().equals(name)) {
                    exist = true;
                    currentProject = project;
                }
            }

            if (!exist) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        List<Task> listTaskRemoving = new ArrayList<>();
        for (Task task : listTasks) {
            if (task.getIdProject().equals(currentProject.getId())){
                listTaskRemoving.add(task);
            }
        }

        listTasks.removeAll(listTaskRemoving);
        System.out.println("Completed");
    }

    public void commandReadAllTasks(List<Task> listTasks, List<Project> listProjects) {
        if (listTasks.size() == 0) {
            System.out.println("We do not have any project");
            return;
        }

        for (Project project : listProjects) {
            printTasksOfProject(project, listTasks);
        }
        System.out.println("Completed");
    }

    public void commandReadTasks(List<Task> listTasks, List<Project> listProjects, Scanner in) {
        if (listProjects.size() == 0){
            System.out.println("We have not any project");
            return;
        }

        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for reading its tasks:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;
            for (Project project : listProjects) {
                if (project.getName().equals(name)) {
                    exist = true;
                    currentProject = project;
                }
            }

            if (!exist) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        printTasksOfProject(currentProject, listTasks);

        System.out.println("Completed");
    }

    public void commandUpdateProject(List<Project> listProjects, Scanner in) {
        Project findProject = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a project name for update:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            for (Project project : listProjects) {
                if (project.getName().equals(name)) {
                    findProject = project;
                }
            }

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = in.nextLine();
        if ("y".equals(answer) || "Y".equals(answer)){
            System.out.println("Input a new name for this task:");
            findProject.setName(in.nextLine());
            System.out.println("Completed");
        }
    }

    public void commandDeleteProject(List<Task> listTasks, List<Project> listProjects, Scanner in) {
        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            for (Project project : listProjects) {
                if (project.getName().equals(name)) {
                    currentProject = project;
                }
            }

            if (Objects.isNull(currentProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }


        List<Task> listTaskRemoving = new ArrayList<>();
        for (Task task : listTasks) {
            if (task.getIdProject().equals(currentProject.getId())){
                listTaskRemoving.add(task);
            }
        }

        listTasks.removeAll(listTaskRemoving);
        listProjects.remove(currentProject);

        System.out.println("Completed");
    }

    public void commandDeleteAllProjects(List<Task> listTasks, List<Project> listProjects) {
        listProjects.clear();
        listTasks.clear();
        System.out.println("Completed");
    }

    public void commandReadProjects(List<Project> listProjects) {
        System.out.println("All projects: \n");
        if (listProjects.size() == 0){
            System.out.println("We have not any project");
            return;
        }

        for (Project project : listProjects) {
            System.out.println();
            System.out.println("Project [" + project.getName() + "]");
            System.out.println("Description: " + project.getDescription());
            System.out.println("ID: " + project.getId());
            System.out.println("Date start: " + project.getDateStart().format(ft));
            System.out.println("Date finish: " + project.getDateFinish().format(ft));
        }

    }

    public void commandCreateProject(List<Project> listProjects, Scanner in) {
        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new project:");
            String name = in.nextLine();

            boolean exist = false;
            for (Project project : listProjects) {
                if (project.getName().equals(name)) {
                    exist = true;
                }
            }

            if (exist) {
                System.out.println("Project with name [" + name + "] already exist");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            currentProject = new Project(name);
        }

        System.out.println("Input a description for this project:");
        currentProject.setDescription(in.nextLine());

        //Date:
        currentProject.setDateStart(inputDate("Input a start date for new project in format<dd-mm-yyyy>:", in));
        currentProject.setDateFinish(inputDate("Input a finish date for new project:", in));

        currentProject.setId(UUID.randomUUID().toString());

        listProjects.add(currentProject);
        System.out.println();
        System.out.println("Project [" + currentProject.getName() + "] has created");
        System.out.println("Description: " + currentProject.getDescription());
        System.out.println("ID: " + currentProject.getId());
        System.out.println("Date start: " + currentProject.getDateStart().format(ft));
        System.out.println("Date finish: " + currentProject.getDateFinish().format(ft));
    }

    public void commandCreateTask(List<Task> listTasks, List<Project> listProjects, Scanner in) {

        Task newTask = new Task();
        boolean idProjectExist = false;

        //IdProject******************
        if (Objects.nonNull(currentProject)) {
            System.out.println("Current project is " + currentProject.getName() + ". Do you want to craete task for this project? <y/n>");
            String answer = in.nextLine();
            if ("y".equals(answer) || "Y".equals(answer)){
                newTask.setIdProject(currentProject.getId());
                idProjectExist = true;
            }
        }

        if (!idProjectExist){
            boolean circleForProject = true;
            while (circleForProject) {
                System.out.println("Input the name of the project for this task:");
                String name = in.nextLine();

                if ("exit".equals(name)){
                    return;
                }

                boolean exist = false;
                for (Project project : listProjects) {
                    if (project.getName().equals(name)) {
                        exist = true;
                        currentProject = project;
                    }
                }

                if (!exist) {
                    System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                    continue;
                }

                circleForProject = false;
                newTask.setIdProject(currentProject.getId());
                System.out.println("OK");
            }
        }
        //IdProject******************

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new task:");
            String name = in.nextLine();

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;

            for (Task task : listTasks) {
                if (task.getName().equals(name)) {
                    exist = true;
                }
            }


            if (exist) {
                System.out.println("Task with name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            newTask.setName(name);

        }


        System.out.println("Input a description for this task:");
        newTask.setDescription(in.nextLine());

        //Date:
        newTask.setDateStart(inputDate("Input a start date for new project in format<dd-mm-yyyy>:", in));
        newTask.setDateFinish(inputDate("Input a finish date for new project:", in));

        newTask.setId(UUID.randomUUID().toString());

        listTasks.add(newTask);
        System.out.println();
        System.out.println("Task [" + newTask.getName() + "] has created");
        System.out.println("Project: " + currentProject.getName());
        System.out.println("Description: " + newTask.getDescription());
        System.out.println("ID: " + newTask.getId());
        System.out.println("Date start: " + newTask.getDateStart().format(ft));
        System.out.println("Date finish: " + newTask.getDateFinish().format(ft));
    }

    private LocalDateTime inputDate(String massage, Scanner in){
        System.out.println(massage);
        String strDate = in.nextLine();
        LocalDateTime date;
        try {
            int year = Integer.parseInt(strDate.substring(6)),
                    month= Integer.parseInt(strDate.substring(3,5)),
                    day  = Integer.parseInt(strDate.substring(0,2));
            date = LocalDateTime.of(year,month, day,0,0);
        }catch (Exception e){
            System.out.println("Error formate date! Date set to the end of year.");
            date = LocalDateTime.of(LocalDateTime.now().getYear(),12,31,23,59);
        }

        return date;
    }

    public void commandHelp() {
        String help = "There are general commands: \n" +
                "   help:                   lists all command\n" +
                "   exit:                   halts the programm\n" +

                "   create-project:         creates new project\n" +
                "   read-projects:          reads all projects\n" +
                "   update-project:         update name of the Nth project\n" +
                "   delete-project:         deletes the project with name <Name>\n" +
                "   delete-all-projects:    deletes all project\n" +

                "   create-task:            creates new task\n" +
                "   read-tasks:             reads tasks for a current project\n" +
                "   read-all-tasks:         reads all tasks\n" +
                "   update-task:            update name of th Nth task\n" +
                "   delete-task:            deletes the task with name <Name> of the cuurrent project\n" +
                "   delete-tasks:           deletes all tasks of a current project\n" +
                "   delete-all-tasks:       deletes all tasks of all projects\n";
        System.out.println(help);
    }


    public void printNotCorrectCommand() {
        System.out.println("not correct command, plz try again or type command <help> \n");
    }

    private void printNotChoosenCurrentProject() {
        System.out.println("There isn't any choosen projects! Plz, choose a project!");
    }
}
