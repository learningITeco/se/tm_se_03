package ru.potapov.tm.repository;

import java.util.List;

public abstract class AbstractRepository<T>  {
    private List<T> list;

    public abstract List<T> findAll();
    public abstract T findOne();
    public abstract void persist(T t);
    public abstract void merge(T t);
    public abstract void remove(T t);
    public abstract void removeAll();
}
