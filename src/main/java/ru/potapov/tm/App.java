package ru.potapov.tm;

import ru.potapov.tm.repository.CommandOfTerminal;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * App ***
 * v 1.0.3
 */
public class App {
    private boolean         exit;
    CommandOfTerminal       terminal;
    private List<Project>   listProjects;
    private List<Task>      listTasks;

    //Constants:
    private final String HELP                   = "help";
    private final String EXIT                   = "exit";

    //Constants for Projects:
    private final String CREATE_PROJECT         = "create-project";
    private final String READ_PROJECTS          = "read-projects";
    private final String UPDATE_PROJECT         = "update-project";
    private final String DELETE_ALL_PROJECTS    = "delete-all-projects";
    private final String DELETE_PROJECT         = "delete-project";

    //Constants for Tasks:
    private final String CREATE_TASK        = "create-task";
    private final String READ_TASKS         = "read-tasks";
    private final String READ_ALL_TASKS     = "read-all-tasks";
    private final String UPDATE_TASK        = "update-task";
    private final String DELETE_TASK        = "delete-task";
    private final String DELETE_TASKS       = "delete-tasks";
    private final String DELETE_ALL_TASKS   = "delete-all-tasks";

    public App() {
        this.exit       = false;
        terminal        = new CommandOfTerminal();
        listProjects    = new ArrayList<>();
        listTasks       = new ArrayList<>();
    }

    public static void main(String[] args) {
        new App().go();
    }

    public void go() {
        Scanner in = new Scanner(System.in);

        while (!exit) {
            System.out.println("\nInsert your command in low case:");
            String strIn = in.nextLine();
            String[] arrCommand = strIn.split(" ");

            chooseCommand(in, arrCommand);
        }
    }

    private void chooseCommand(Scanner in, String... arrCommand) {
        String cmd = arrCommand[0];

        switch (cmd){
            case HELP:                  terminal.commandHelp(); break;
            case EXIT:                  commandExit(); break;
            case CREATE_PROJECT:        terminal.commandCreateProject(listProjects, in); break;
            case READ_PROJECTS:         terminal.commandReadProjects(listProjects); break;
            case DELETE_ALL_PROJECTS:   terminal.commandDeleteAllProjects(listTasks, listProjects); break;
            case DELETE_PROJECT:        terminal.commandDeleteProject(listTasks, listProjects, in); break;
            case UPDATE_PROJECT:        terminal.commandUpdateProject(listProjects, in); break;
            case CREATE_TASK:           terminal.commandCreateTask(listTasks, listProjects, in); break;
            case READ_TASKS:            terminal.commandReadTasks(listTasks,listProjects,in); break;
            case READ_ALL_TASKS:        terminal.commandReadAllTasks(listTasks, listProjects); break;
            case DELETE_TASK:           terminal.commandDeleteTask(listTasks, in); break;
            case DELETE_TASKS:          terminal.commandDeleteTasks(listTasks,listProjects, in); break;
            case DELETE_ALL_TASKS:      terminal.commandDeleteAllTasks(listTasks); break;
            case UPDATE_TASK:           terminal.commandUpdateTask(listTasks,listProjects,in); break;
            default:                    terminal.printNotCorrectCommand();break;

        }
    }

    private void commandExit() {
        System.out.println("Buy-buy....");
        exit = true;
    }
}
